package com.crm.qa.testcases;

import org.openqa.selenium.By;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.LoginPage;

public class LoginPageTest extends TestBase {
	LoginPage loginpage;
	HomePage homepage;
	

	@BeforeClass
	public void setup()
	{
		initialization();
		loginpage = new LoginPage();
	}
	
	@Test(priority=0)
	public void shopcluselogin() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"sign-in\"]")).click();
		Thread.sleep(5000);
		driver.findElement(By.xpath("/html/body/div[1]/div/div/div[3]/div[1]/button[1]")).click();
		Thread.sleep(5000);
		
		homepage = loginpage.login(prod.getProperty("username"), prod.getProperty("password"));
	}
	@Test(priority=1)
	public void clickOnprofile() throws InterruptedException
	{
		Thread.sleep(5000);
		driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/a")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"sc_uname\"]/div/ul/li[4]/a")).click();
		
	}

	@AfterClass
	  public void afterClass() {
	  }

}
